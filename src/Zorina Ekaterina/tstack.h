#ifndef __TSTACK_H__
#define __TSTACK_H__

#include "tdataroot.h"

class TStack: public TDataRoot
{
protected:
	int Li;		//������ ���������� �������� �����
public:
	TStack(int Size = DefMemSize): TDataRoot(Size), Li(-1) {}
	virtual void  Put(const TData &val);	//�������� �������
	virtual TData Get();					//����� �������
	TData Top();							//�������� ���������� ��-�� �����

	//��������� ������
	virtual int IsValid() {return 0;};		//������������ ���������
	virtual void Print();					//������ �������� �����
};

#endif