#include "tqueue.h"


class TJobStream : public TQueue
{
private:
	int lastID;					//����� ���������� ������������ � ����� �������
	int q1;						//����������� ��������� ������ �������
	int pr;						//����������� ����, ��� ��� ����� � ������� �����������
public:
	long refusalcount = 0;		//������� �������
	long throwawaycount = 0;	//������� �������
	long highprioritycount = 0;	//������� ��������
	TJobStream(int q1, int pr, int qsize);
	void CreateJob();
	int GetLastID() const { return lastID; };

};
