#include "TProc.h"
#include <time.h>
#include <locale>
#include <iostream>
using namespace std;

TProc::TProc(int _q1, int _q2, int _pr, long _tacts, int _qsize) : jobstream(_q1, _pr, _qsize)
{
	totaltacts = _tacts;
	q2 = _q2;
	downtimecount = 0;
	isEmpty = true;
	curID = 0;
}

void TProc::DoProc()
{
	for (long i = 0; i < totaltacts; i++)
	{
		jobstream.CreateJob();
		if (isEmpty)
		{
			if (jobstream.IsEmpty())
				downtimecount++;
			else
			{
				jobstream.Get();
				isEmpty = false;
			}
		}
		else
		{
			int _q2 = (rand() % 100);
			if (_q2 < q2)
				isEmpty = true;
		}
	}
}

void TProc::Print()
{
	cout << endl;
	cout << "���������� ����������� � �� ������� � ������� ����� �������� ��������: " << jobstream.GetLastID() << endl;
	cout << "�� ��� � ������� �����������: " << jobstream.highprioritycount << endl;
	cout << "���������� ������� � ������������ ������� ��-�� ������������ �������: " << jobstream.refusalcount << endl;
	cout << "���������� '������������' ��-�� ������� � ������� �����������: " << jobstream.throwawaycount << endl;
	cout << "���������� ������ �������: " << downtimecount << endl;
}
