#include "tqueue.h"
#include <iostream>
using namespace std;


TData TQueue::Get(void)
{
	if (pMem == nullptr)
		throw SetRetCode(DataNoMem);
	if (IsEmpty())	
		throw SetRetCode(DataEmpty);
	DataCount--;
	TData temp;
	temp = pMem[Fi];
	Fi = ++Fi % MemSize;
	
	return temp;
}

void TQueue::Put(const TData &val)
{
	if (pMem == nullptr)
		throw SetRetCode(DataNoMem);
	if (IsFull())
		throw SetRetCode(DataFull);
	else
	{
		Li = ++Li % MemSize;
		pMem[Li] = val;
		DataCount++;
	}

}

void TQueue::PutInHead(const TData &val)
{
	if (pMem == nullptr)
		throw SetRetCode(DataNoMem);

	TData* temp;
	int tsize = 100 + 1;
	bool full = IsFull();
	temp = new TData[tsize];
	temp[0] = val;
	int i = 1;
	while (!IsEmpty())
	{ 
		temp[i] = Get();
		i++;
	}
	if (i == 1)
		Put(temp[0]);
	else
	while (!IsFull() && i >= 0)
	{
		Put(temp[i]);
		i--;
	}
	if (!full)
		DataCount++;
		
}


void TQueue::Print() 
{
	if (DataCount > 0)
		for (int i = 0; i < DataCount; i++)
			std::cout<< pMem[(i + Fi) % MemSize] << ' ';
	cout << endl;
}