#ifndef __TQUEUE_H__
#define __TQUEUE_H__

#include "tstack.h"

class TQueue : public TStack
{
private:
	int Fi;		//������ ������� � �������
public:
	TQueue(int size = DefMemSize) : TStack(size) { Fi = 0; }
	virtual TData Get();		//����� �������
	virtual void Put(const TData &val);
	virtual void PutInHead(const TData &val);
	virtual void Print();		//������ ��������� �������
};

#endif