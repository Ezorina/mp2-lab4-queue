## Методы программирования 2: Очередь
### Цели и задачи
Лабораторная работа направлена на практическое освоение динамической структуры данных Очередь. С этой целью в лабораторной работе изучаются различные варианты структуры хранения очереди и разрабатываются методы и программы решения задач с использованием очередей. В качестве области приложений выбрана тема эффективной организации выполнения потока заданий на вычислительных системах.

Очередь характеризуется таким порядком обработки значений, при котором вставка новых элементов производится в конец очереди, а извлечение – из начала. Подобная организация данных широко встречается в различных приложениях. В качестве примера использования очереди предлагается задача разработки системы имитации однопроцессорной ЭВМ. Рассматриваемая в рамках лабораторной работы схема имитации является одной из наиболее простых моделей обслуживания заданий в вычислительной системе и обеспечивает тем самым лишь начальное ознакомление с проблемами моделирования и анализа эффективности функционирования реальных вычислительных систем.

**Очередь (англ. queue)**, – схема запоминания информации, при которой каждый вновь поступающий ее элемент занимает крайнее положение (конец очереди). При выдаче информации из очереди выдается элемент, расположенный в очереди первым (начало очереди), а оставшиеся элементы продвигаются к началу; следовательно, элемент, поступивший первым, выдается первым.
#### Требования к лабораторной работе
Для вычислительной системы (ВС) с одним процессором и однопрограммным последовательным режимом выполнения поступающих заданий требуется разработать программную систему для имитации процесса обслуживания заданий в ВС. При построении модели функционирования ВС должны учитываться следующие основные моменты обслуживания заданий: 
- генерация нового задания; 
- постановка задания в очередь для ожидания момента освобождения процессора;
- выборка задания из очереди при освобождении процессора после обслуживания очередного задания.

По результатам проводимых вычислительных экспериментов система имитации должна выводить информацию об условиях проведения эксперимента (интенсивность потока заданий, размер очереди заданий, производительность процессора, число тактов имитации) и полученные в результате имитации показатели функционирования вычислительной системы, в т.ч. 
- количество поступивших в ВС заданий; 
- количество отказов в обслуживании заданий из-за переполнения очереди; 
- среднее количество тактов выполнения заданий; 
- количество тактов простоя процессора из-за отсутствия в очереди заданий для обслуживания.
- 
Показатели функционирования вычислительной системы, получаемые при помощи систем имитации, могут использоваться для оценки эффективности применения ВС; по результатам анализа показателей могут быть приняты рекомендации о целесообразной модернизации характеристик ВС (например, при длительных простоях процессора и при отсутствии отказов от обслуживания заданий желательно повышение интенсивности потока обслуживаемых заданий и т.д.).

#### Основные допущения
1. При планировании очередности обслуживания заданий будет учитываться возможность задания приоритетов.
2. Моменты появления новых заданий и моменты освобождения процессора рассматриваются как случайные события.

#### Результатом выполнения работы является следующий набор файлов:
- **TStack.h, TStack.cpp** – модуль с классом, реализующим операции над стеком (был взят из предыдущей лабороторной работы);
- **TQueue.h, TQueue.cpp** – модуль с классом, реализующим операции над очередью;
- **TProc.h, TProc.cpp** – модуль с классом, реализующим процессор;
- **TJobStream.h, TJobStream.cpp** – модуль с классом, реализующим поток задач;
- **QueueTestkit.cpp** – модуль программы тестирования.
- 

### Выполнение работы:
#### Реализация класса TStack

```
#include "tdataroot.h"

class TStack: public TDataRoot
{
protected:
	int Li;		//индекс последнего элемента стека
public:
	TStack(int Size = DefMemSize): TDataRoot(Size), Li(-1) {}
	virtual void  Put(const TData &val);	//добавить элемент
	virtual TData Get();					//взять элемент
	TData Top();							//значение последнего эл-та стека

	//служебные методы
	virtual int IsValid() {return 0;};		//тестирование структуры
	virtual void Print();					//печать значений стека
};

void TStack::Put(const TData &val) 
{
	if (pMem == nullptr) 
		throw SetRetCode(DataNoMem);
	else if (IsFull())
		SetMem(pMem, MemSize + DefMemSize);
	pMem[++Li] = val;
	DataCount++;
}

TData TStack::Get(void)
{
	if (pMem == nullptr)
		throw SetRetCode(DataNoMem);
	if (IsEmpty())	
		throw SetRetCode(DataEmpty);
	DataCount--;
	return pMem[Li--];
}

TData TStack::Top()
{
	if (pMem == nullptr)
		throw SetRetCode(DataNoMem);
	if (IsEmpty())
		SetRetCode(DataEmpty);
	return pMem[Li];
}

void TStack::Print() 
{
	if (DataCount > 0)
		for (int i = 0; i < DataCount; i++)
			cout << pMem[i] << " ";
	cout << endl;
}
```

#### Реализация класса TQueue на основе класса TStack

```
#include "tstack.h"

class TQueue : public TStack
{
private:
	int Fi;		//Первый элемент в очереди
public:
	TQueue(int size = DefMemSize) : TStack(size) { Fi = 0; }
	virtual TData Get();		//взять элемент
	virtual void Put(const TData &val);
	virtual void PutInHead(const TData &val);
	virtual void Print();		//Печать элементов очереди
};

TData TQueue::Get(void)
{
	if (pMem == nullptr)
		throw SetRetCode(DataNoMem);
	if (IsEmpty())	
		throw SetRetCode(DataEmpty);
	DataCount--;
	TData temp;
	temp = pMem[Fi];
	Fi = ++Fi % MemSize;
	
	return temp;
}

void TQueue::Put(const TData &val)
{
	if (pMem == nullptr)
		throw SetRetCode(DataNoMem);
	if (IsFull())
		throw SetRetCode(DataFull);
	else
	{
		Li = ++Li % MemSize;
		pMem[Li] = val;
		DataCount++;
	}

}

void TQueue::PutInHead(const TData &val)
{
	if (pMem == nullptr)
		throw SetRetCode(DataNoMem);

	TData* temp;
	int tsize = MemSize + 1;
	bool full = IsFull();
	temp = new TData[tsize];
	temp[0] = val;
	int i = 1;
	while (!IsEmpty())
	{ 
		temp[i] = Get();
		i++;
	}
	if (i == 1)
		Put(temp[0]);
	else
	while (!IsFull() && i >= 0)
	{
		Put(temp[i]);
		i--;
	}
	if (!full)
		DataCount++;
		
}


void TQueue::Print() 
{
	if (DataCount > 0)
		for (int i = 0; i < DataCount; i++)
			std::cout<< pMem[(i + Fi) % MemSize] << ' ';
	cout << endl;
}
```

#### Тестирование класса TQueue

```
TEST(TQueue, can_create_queue_with_positive_length) 
{
	ASSERT_NO_THROW(TQueue q(5));
}

TEST(TQueue, throws_when_create_queue_with_negative_length)
{
	ASSERT_ANY_THROW(TQueue q(-1));
}

TEST(TQueue, created_queue_is_empty) 
{
	TQueue q(4);

	EXPECT_TRUE(q.IsEmpty());
}

TEST(TQueue, can_put_element)
{
	TQueue q;
	q.Put(0);

	EXPECT_FALSE(q.IsEmpty());
}

TEST(TQueue, can_get_element)
{
	TQueue q;
	int num = 6;
	q.Put(num);

	EXPECT_EQ(num, q.Get());
}

TEST(TQueue, cant_get_from_empty_queue)
{
	TQueue q;

	EXPECT_ANY_THROW(q.Get());
}

TEST(TQueue, can_put_element_to_full_queue)
{
	TQueue q(1);
	q.Put(1);
	q.Put(2);

	EXPECT_EQ(1, q.Get());
}

TEST(TQueue, get_returns_first_placed_elem)
{
	TQueue q;
	q.Put(1);
	q.Put(2);
	q.Put(3);

	EXPECT_EQ(1, q.Get());
}

TEST(TQueue, can_put_elem_in_head)
{
	TQueue q;
	q.Put(1);
	q.PutInHead(2);

	EXPECT_EQ(2, q.Get());
}

```

[](./tmp/tests.jpg)

### Имитация модели вычислительной системы
Возможная простая схема имитации процесса поступления и обслуживания заданий в вычислительной системе состоит в следующем. - Каждое задание в системе представляется некоторым однозначным идентификатором (например, порядковым номером задания). - Для проведения расчетов фиксируется (или указывается в диалоге) число тактов работы системы. - На каждом такте опрашивается состояние потока задач и процессор. - Регистрация нового задания в вычислительной системе может быть сведена к запоминанию идентификатора задания в очередь ожидания процессора. Ситуацию переполнения очереди заданий следует понимать как нехватку ресурсов вычислительной системы для ввода нового задания (отказ от обслуживания). - Для моделирования процесса обработки заданий следует учитывать, что процессор может быть занят обслуживанием очередного задания, либо же может находиться в состоянии ожидания (простоя). - В случае освобождения процессора предпринимается попытка его загрузки. Для этого извлекается задание из очереди ожидания. - Простой процессора возникает в случае, когда при завершении обработки очередного задания очередь ожидающих заданий оказывается пустой.

После проведения всех тактов имитации производится вывод характеристик вычислительной системы: - количество поступивших в вычислительную систему заданий в течение всего процесса имитации; - количество отказов в обслуживании заданий из-за переполнения очереди – целесообразно считать процент отказов как отношение количества не поставленных в очередь заданий к общему количеству сгенерированных заданий, умноженное на 100%; - среднее количество тактов выполнения задания; - количество тактов простоя процессора из-за отсутствия в очереди заданий для обслуживания – целесообразно считать процент простоя процессора как отношение количества тактов простоя процессора к общему количеству тактов имитации, умноженное на 100%.

#### Класс TJobStream

```
class TJobStream : public TQueue
{
private:
	int lastID;					//номер последнего поступившего в поток задания
	int q1;						//вероятность появления нового задания
	int pr;						//вероятность того, что оно будет с высоким приоритетом
public:
	long refusalcount = 0;		//счётчик отказов
	long throwawaycount = 0;	//счётчик отказов
	long highprioritycount = 0;	//счётчик выбросов
	TJobStream(int q1, int pr, int qsize);
	void CreateJob();
	int GetLastID() const { return lastID; };

};

TJobStream::TJobStream(int _q1, int _pr, int _qsize) : TQueue(_qsize)
{
	lastID = 0;
	q1 = _q1;
	pr = _pr;
	srand(time(NULL));
}

void TJobStream::CreateJob()
{
	int _q1 = (rand() % 100);
	int _pr = (rand() % 100);
	if (_q1 < q1)
	{
		lastID++;
		if (!IsFull())
			if (_pr >= pr)
				Put(lastID);
			else
			{
				PutInHead(lastID);
				highprioritycount++;
			}
		else
		{
			if (_pr < pr)
			{
				PutInHead(lastID);
				highprioritycount++;
				throwawaycount++;
			}
			refusalcount++;
		}
	}
}

```

#### Класс TProc

```
class TProc
{
private:
	int q2;				//вероятность завершения выполнения задания
	long totaltacts;	//всего тактов
	int curID;			//номер выполняемого задания
	bool isEmpty;
	TJobStream jobstream;
	long downtimecount;	//счётчик простоев
public:
	TProc(int q1, int q2, int pr, long tacts, int qsize);
	void DoProc();
	void Print();
};

TProc::TProc(int _q1, int _q2, int _pr, long _tacts, int _qsize) : jobstream(_q1, _pr, _qsize)
{
	totaltacts = _tacts;
	q2 = _q2;
	downtimecount = 0;
	isEmpty = true;
	curID = 0;
}

void TProc::DoProc()
{
	for (long i = 0; i < totaltacts; i++)
	{
		jobstream.CreateJob();
		if (isEmpty)
		{
			if (jobstream.IsEmpty())
				downtimecount++;
			else
			{
				jobstream.Get();
				isEmpty = false;
			}
		}
		else
		{
			int _q2 = (rand() % 100);
			if (_q2 < q2)
				isEmpty = true;
		}
	}
}

void TProc::Print()
{
	cout << endl;
	cout << "Количество поступивших в ВС заданий в течение всего процесса имитации: " << jobstream.GetLastID() << endl;
	cout << "Из них с высоким приоритетом: " << jobstream.highprioritycount << endl;
	cout << "Количество отказов в обслуживании заданий из-за переполнения очереди: " << jobstream.refusalcount << endl;
	cout << "Количество 'пострадавших' из-за заданий с высоким приоритетом: " << jobstream.throwawaycount << endl;
	cout << "Количество тактов простоя: " << downtimecount << endl;
}

```

#### Демонстрационная программа

```
#include <iostream>
#include <time.h>
#include "TProc.h"
using namespace std;

void main()
{
	setlocale(LC_ALL, "Russian");
	try
	{
		TProc proc(20, 20, 20, 100000, 20);
		proc.DoProc();
		proc.Print();
		system("Pause");
	}
	catch (int num)
	{
		cout << num << endl;
		system("Pause");
	}
}

```

[](./tmp/sample.jpg)

#### Исследование вычислительной системы

В качестве исследования данной модели предлагается рассмотреть влияние поочередного изменения различных параметров на ее характеристики. При каждом запуске работа системы длилась 100 000 тактов.

[](./tmp/table.jpg)

#### Вывод

В ходе выполнения данной работы мы поработали со структурой данных "очередь". Класс `TQueue` был реализован на основе ранее разработанного класса `TStack`, а также был использован при написании имитации модели вычислительной системы. С его помощью был реализован класс TJobStream, реализующий поток задач для процессора.